
import spark.Request
import spark.Response
import groovy.util.logging.Slf4j


@Slf4j
public class App extends SparkGroovy {

    def init() {

      port System.getProperty('server.port', '8080').toInteger()

      get "/hello/:name", { Request request, Response response ->
        return "Hello " + request.params('name') + "\n"
      }

      after { Request request, Response response ->
        log.info("${request.requestMethod()} ${request.pathInfo()}")
      }

    }

    public static void main(String[] args) {
      def app = new App()

      log.info("Initialize App")

      app.init()
    }
}
